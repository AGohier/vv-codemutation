
# 1 - Introduction

### - Context of the project:
   A test suit shall detect efficiently wrong behaviors of the code.
   Writing pertinent test isn't an easy task an gives several challenges to address.
   One is to make sure to handel in tests all the tree of case proposed in the code. 
   Another is to check the relevance of a test to ensure the correct behavior of a method or a class.
### - Synthetic description of the problem you address:
   The present project check the ability of the test suit to detect artificial malfunctions 
   introduced in the target project's code. The surviving mutants we will be reported to give a focus on 
   code fragments that could need a better test coverage or more relevant tests.
### - Main points of your solution (originality, algorithm, assembling several techniques, etc.)
   Our solution take as input the address of a Maven repository to display a report in the same console.
   It doesn't need the project to be an actual Maven project. It just needs to make available the sources
   of the project using Maven's standards. Our implementation stops the test suite execution at the first fail.
   those particularities are allowed thanks to a the use of instantiation of loaders directly in the project's flow
   without delegation to Maven. 
   
# 2 - Solution
### - Describe the global approach, illustrate with a figure / workflow

<img src="./reportResources/generalSchema.png"  width="900">

   In our approch, a MainExec handle the global steps of the workfow: initialize, inspect code, 
   before editing the report.
   
   An Inspector will loop on classes and method to detect candidates for mutation.
   It instantiates mutators in accordingly to the candidate, and gives it the context of its job.
   Mutators has been developed using a **Strategy deign pattern bend with a Template Method**.
   The inspector then call for an execution of the evaluated project test suit before the addition
   of the mutant to the final report if it survived.
   
   After a full inspection of the code, a report is then edited displaying the number of surviving mutants
   and their related details.


### - Main technical elements (core algorithm, main ideas)

   Our implementation of this project makes use of javassist to mutate classes. 
   If a test fails during the execution of the test suit, then the execution stops right away.
   
   - Implemented mutations:
        - False/True: 
        
            If a method returns a boolean value, then two mutations are tested, an empty method returning
            true, and another returning false.
        
        - Void:
        
            If a method is void, then a mutation is tested where the method body is empty( "{}" ).
        
        - Operator:
            
            This mutation invert operation operators:
            - If a "+" is detected, then a "-" is tested.
            - If a "-" is detected, then a "+" mutant is tested.
            - If a "/" is detected, then a "*" mutant is tested.
            - If a "*" is detected, then a "/" mutant is tested.

   - Sequence of a mutation:
        - Global sequence:
        
        This diagram details the global logic of our project. It gives the sequence of actions
        the user will trigger after providing the address of his project to the console.
        <img src="./reportResources/generalSequencialDiagram.png"  width="900">
            
        Our entry point is the class MainExec. It ask the user through the console.
        It the load the classes into a javassist ClassPool witch will be used to inspect the all
        project bytecode.
        
        A data structure SurvivingMutant is then instantiate. (It will store information for a later report edition) 
        
        The actual job of inspecting the code, generating mutants, and executing test is delegated to the Inspector.
        
        After the Inspector is finished with if task, the MainExec then calls the SurvivingMutants structure to print
        the report before the end of execution.
        
        - Mutation sequence:
        This diagram detail the first mutation of a new class. The Inspector generate a single backup for all 
        the mutations attempts into a class. It then loop on every methods to inspect.        
        <img src="./reportResources/inspectionSequencialDiagram.png"  width="900">

        The first step is to instantiate a FileBackup. It reads the original class from the disk and makes this backup 
        ready for a further restoration.

        When a possible mutation is detected, the corresponding Mutator is instantiated to initiate the mutation. 
        It will use a child ClassPool to mutate the method and write the mutated class.
        
        Then, an execution of the project test suit is called through TestExecutor. It will use an UrlClassLoader to 
        load project and test classes to execute them using JUnit4. Depending of the success of the full test suit, 
        the TestExecutor will respond True or False to the Inspector.
        
        The Inspector then tests is the mutant survived, If so, the mutant details will be sent to SurvivingMutants.
        
        The Inspector calls then the FileBackup to restore the original class on the disk.
        
        Following this sequence, the Inspector will continue it analyse of the code until a path on the total code is 
        achieved.

### - Implementation: architecture, tools that have been integrated, technical choices, languages
Java, JUnit4, Javassist
### - Elements of documentation to use the tool (screenshots, example of usage)
 - After execution of this project, the console will ask you to provide the root folder of your Maven project 
with the format provided as example ("/home/your/project/is/here/Project")

 - After inspection of the sources and test of mutants, a report of surviving mutants is then displayed.
### - Software engineering: version control, testing, documentation, iterations, etc.
 - The present project make use of Git for version control.
 - JUnit 4 is used for tests.
# 3 - Evaluation
### - Description of subjects (what programs you used to test your technique, what size, etc.)
# 4 - Discussion
### What has been achieved
 - We achieved the loading of a project to manipulate it code and writing it back to the disk.
 - We achieved to execute a test suit form an external project.
 - We produced an algorithm able to inspect in an exhaustive way a Maven code project to detect specific elements.
#### Problems encountered
  - We encountered difficulties to handel the concepts needed to successfully deploy javassist mutations and
   to execute the test suite. Most of our time spent on this project has been an exploration of the documentation and 
    failed experiments toward a solution to solve this problem.
  - We did not achieved to implement an proper user interface.
  - We encountered issues to synchronize our work in reason of diverging time schedules (IL/ILA)
### How the solution can be extended in the future
 - A proper graphic interface could be developed.
 - A more complete setup of possible mutations could be achieved.
 - The report could be wrote into a csv folder.
 - This mutator project detect poorly tested code, it could be connected to a test generator to 
 automatically generate a test in order to kill reported mutants.

# 5 - Conclusion

We achieved a functional mutation tester to evaluate the testing suit of a Maven Project. The time spend assimilating 
the necessary concepts made this final outcome quite rustic. But it has been for us the opportunity to understand the 
concepts supporting the class loading at runtime.


   
