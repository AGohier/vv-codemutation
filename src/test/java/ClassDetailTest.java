import fr.istic.dataStructure.ClassDetail;
import org.junit.Assert;
import org.junit.Test;

public class ClassDetailTest {

    @Test
    public void testConsistency(){
        ClassDetail classDetail = new ClassDetail("Path","Name");
        Assert.assertEquals("Test Path",classDetail.getPathToClass(),"Path");
        Assert.assertEquals("Test Name",classDetail.getClassName(),"Name");
    }
}
