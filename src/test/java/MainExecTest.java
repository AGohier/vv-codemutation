import fr.istic.MainExec;
import javassist.CannotCompileException;
import javassist.NotFoundException;
import org.junit.Assert;
import org.junit.Test;
import org.apache.commons.io.FileUtils;

import java.io.*;

public class MainExecTest {


    @Test
    public void testConsistency() throws ClassNotFoundException, CannotCompileException, NotFoundException, IOException {
        File output = new File("TestingMaterial/out/output.txt");
        // clean previous writings
        PrintWriter writer = new PrintWriter(output);
        writer.print("");
        writer.close();
        // setting output stream
        PrintStream out = new PrintStream(new FileOutputStream("TestingMaterial/out/output.txt"));
        System.setOut(out);

        File file = new File("TestingMaterial/bidon");
        String absolutePath=file.getAbsolutePath();
        ByteArrayInputStream in = new ByteArrayInputStream(absolutePath.getBytes());
        System.setIn(in);
        // executing test
        MainExec.main(new String[]{});

        File compare = new File("TestingMaterial/out/compare.txt");


        Assert.assertEquals("Fail to test MainExec",
                FileUtils.readFileToString(output, "utf-8"),
                FileUtils.readFileToString(compare, "utf-8"));

        // optionally, reset System.in to its original
        System.setIn(System.in);
    }



}
