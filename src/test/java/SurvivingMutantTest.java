import fr.istic.dataStructure.MutantEntry;
import fr.istic.dataStructure.SurvivingMutant;
import org.junit.Assert;
import org.junit.Test;

import java.util.List;

public class SurvivingMutantTest {


    @Test
    public void testConsistency(){
        MutantEntry mutantEntry1 = new MutantEntry("className1","methodName1","mutationType1",1);
        MutantEntry mutantEntry2 = new MutantEntry("className2","methodName2","mutationType2",2);
        SurvivingMutant survivingMutant = new SurvivingMutant();
        Assert.assertEquals("Test SurvivingMutant is empty",survivingMutant.sizeOfMutantList(),0);
        survivingMutant.add(mutantEntry1.getClassName(),mutantEntry1.getMethodName(),mutantEntry1.getType(),mutantEntry1.getPosition());
        Assert.assertEquals("Test SurvivingMutant contain one element",survivingMutant.sizeOfMutantList(),1);
        survivingMutant.add(mutantEntry2.getClassName(),mutantEntry2.getMethodName(),mutantEntry2.getType(),mutantEntry2.getPosition());
        Assert.assertEquals("Test SurvivingMutant contain two elements",survivingMutant.sizeOfMutantList(),2);
        List<MutantEntry> list = survivingMutant.getMutantEntryList();
        Assert.assertEquals("Test SurvivingMutant list",list.get(0).getClassName(),mutantEntry1.getClassName());
        Assert.assertEquals("Test SurvivingMutant list",list.get(1).getClassName(),mutantEntry2.getClassName());
    }

    @Test
    public void testKillingMutant(){
        SurvivingMutant survivingMutant = new SurvivingMutant();
        Assert.assertEquals("Test initial killed mutant count",survivingMutant.getKilledMutantCount(),0);
        survivingMutant.justKilledAMutant();
        Assert.assertEquals("Test kill counter incremental",survivingMutant.getKilledMutantCount(),1);
        survivingMutant.justKilledAMutant();
        Assert.assertEquals("Test kill counter incremental",survivingMutant.getKilledMutantCount(),2);
    }

}
