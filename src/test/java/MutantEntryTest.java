import fr.istic.dataStructure.MutantEntry;
import org.junit.Assert;
import org.junit.Test;

public class MutantEntryTest {

    @Test
    public void testConsistency(){
        MutantEntry mutantEntry = new MutantEntry("className","methodName","mutationType",9);
        Assert.assertEquals("Test className",mutantEntry.getClassName(),"className");
        Assert.assertEquals("Test methodName",mutantEntry.getMethodName(),"methodName");
        Assert.assertEquals("Test mutationType",mutantEntry.getType(),"mutationType");
        Assert.assertEquals("Test methodName",mutantEntry.getPosition(),9);
    }

}
