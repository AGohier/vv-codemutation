package fr.istic.mutator;

import javassist.*;


public abstract class MutatorPrototype {

    /**
     * Return a ClassPool were the specified class is mutated
     * @param classPool
     * @param className
     * @param methodName
     * @param position
     * @return
     */
    public ClassPool mutate(ClassPool classPool, String className, String methodName, int position){
        CtClass ctClass;
        CtMethod ctMethod = null;
        try {
            ctClass = classPool.get(className);
            ctMethod = ctClass.getDeclaredMethod(methodName);
        } catch (NotFoundException e) { e.printStackTrace(); }

        ctMethodMutation(ctMethod,position);
        return  classPool;
        }

    /**
     * Method to implement to change the method according to instructions
     * @param ctMethod
     * @param position
     */
    abstract protected void ctMethodMutation (CtMethod ctMethod, int position);
}
