package fr.istic.mutator;

import javassist.CannotCompileException;
import javassist.CtMethod;

public class MutatorVoid extends MutatorPrototype {

    @Override
    protected void ctMethodMutation(CtMethod ctMethod, int position) {
    	try {
			ctMethod.setBody("{}");
		} catch (CannotCompileException e) {
			e.printStackTrace();
		}
    }

}