package fr.istic.mutator;

import javassist.CannotCompileException;
import javassist.CtMethod;

public class MutatorTrue extends MutatorPrototype {

    @Override
    protected void ctMethodMutation(CtMethod ctMethod, int position) {
    	try {
			ctMethod.setBody("{return true;}");
		} catch (CannotCompileException e) {
			e.printStackTrace();
		}
    }

}