package fr.istic.mutator;

import javassist.CannotCompileException;
import javassist.CtMethod;

public class MutatorFalse extends MutatorPrototype {

    @Override
    protected void ctMethodMutation(CtMethod ctMethod, int position) {
    	try {
			ctMethod.setBody("{return false;}");
		} catch (CannotCompileException e) {
			e.printStackTrace();
		}
    }

}