package fr.istic.mutator;

import javassist.CtMethod;
import javassist.bytecode.Opcode;

public class MutatorOperator extends MutatorPrototype {
	// TODO
	@Override
	protected void ctMethodMutation(CtMethod ctMethod, int position) {
		byte[] code = ctMethod.getMethodInfo().getCodeAttribute().getCode();
		if(code[position] == Opcode.DMUL) {
			code[position] = Opcode.DDIV;
		}
		else if(code[position] == Opcode.DDIV) {
			code[position] = Opcode.DMUL;
		}
		else if(code[position] == Opcode.DADD) {
			code[position] = Opcode.DSUB;
		}
		else if(code[position] == Opcode.DSUB) {
			code[position] = Opcode.DADD;
		}
	}

}