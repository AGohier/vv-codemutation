package fr.istic;

import fr.istic.dataStructure.ClassDetail;
import fr.istic.dataStructure.SurvivingMutant;
import fr.istic.logic.Inspector;
import javassist.CannotCompileException;
import javassist.ClassPool;
import javassist.NotFoundException;


import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;

public class MainExec {

public static void main(String[] args) throws IOException, ClassNotFoundException, NotFoundException, CannotCompileException {

    System.out.println("Enter your Maven project root path here like : \"/home/your/project/is/here/Project\"");
    Scanner scanner = new Scanner(System.in);
    String option = scanner.nextLine();
    // System.out.println(option);

    String pathToClasses;
    String pathToTests;

    // name of the class launching the test suit
//    String mainTestClass = "EditorEngineTest";

    pathToClasses = option+"/target/classes";
    pathToTests = option+"/target/test-classes";


    List<ClassDetail> classes = getClassList(pathToClasses);

    // generating the ClassPool
    ClassPool pool = ClassPool.getDefault();
    pool.appendClassPath(pathToClasses);

    // generate the mutation inspector
    SurvivingMutant survivingMutant = new SurvivingMutant();

    // test the tests execution from the MainExec
    // TestExecutor.runTests(pathToClasses,pathToTests);

    Inspector inspector = new Inspector(pathToClasses,pathToTests,classes,pool,survivingMutant);
    inspector.inspect();

    // print report in the console
    survivingMutant.displayReport();

}

    public static List<ClassDetail> getClassList(String classPath){
        String path = classPath;
        List<String> tempList = new ArrayList<String>();
        List<ClassDetail> toReturnList = new ArrayList();
        iterDirectory(path,tempList);
        for(String str :tempList){
            if(str.endsWith(".class")){
                // System.out.println(str);
                String tempStr = str.substring(0,str.length()-6);
                tempStr = tempStr.substring(path.length()+1,tempStr.length());
                tempStr = tempStr.replace('/', '.');
                toReturnList.add(new ClassDetail(str,tempStr));
            }
        }
        return toReturnList;
    }

    private static void iterDirectory(String repertoire, List<String> classes) {
        File dir = new File(repertoire);
        //Once you have the appropriate path, you can iterate through its contents:
        //List directory
        // si le repertoire courant est bien un repertoire
        if(dir.isDirectory()){
            String s[] = dir.list();
            for (String str :s){

                File dirTemp = new File(repertoire +"/"+ str  );
                // si le terme de la liste est lui-même un répertoire
                if(dirTemp.isDirectory()){
                    iterDirectory(repertoire +"/"+ str , classes);
                }
                // si le terme de la liste est un fichier
                if(!dirTemp.isDirectory()){
                    classes.add(repertoire+"/"+str);
                }
            }
        }
    }

}
