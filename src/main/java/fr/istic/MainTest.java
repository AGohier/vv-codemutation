package fr.istic;

import javassist.ClassPool;
import javassist.CtClass;
import javassist.NotFoundException;

import java.io.File;
import java.net.MalformedURLException;
import java.net.URL;
import java.net.URLClassLoader;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

public class MainTest {

    public static void main(String[] args) throws NotFoundException, MalformedURLException, ClassNotFoundException {

        ClassPool pool = ClassPool.getDefault();
        pool.appendClassPath("/home/arnaud/Documents/V&V/Dummy/out/production/Dummy");
        CtClass ctClass = pool.get("memento.Memento");
        System.out.println(ctClass.getName());

        URL[] urls = {new URL("file:///home/arnaud/Documents/V&V/Dummy/out/production/Dummy/")};
        URLClassLoader urlClassLoader = URLClassLoader.newInstance(urls);
        Class<?> c = urlClassLoader.loadClass("memento.Memento");
        System.out.println(c.getName());





    }



}



