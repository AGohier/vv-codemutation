package fr.istic.logic;

import fr.istic.MainExec;
import fr.istic.dataStructure.ClassDetail;
import org.junit.runner.*;
import org.junit.runner.notification.RunNotifier;
import org.junit.runner.notification.StoppedByUserException;

import java.net.MalformedURLException;
import java.net.URL;
import java.net.URLClassLoader;
import java.util.ArrayList;
import java.util.List;

public class TestExecutor {
    public static boolean runTests(String pathToClasses,String pathToTests) {

        String usedClasses = "file://"+pathToClasses+"/";
        String usedTests = "file://"+pathToTests+"/";

        URL[] urls = new URL[0];
        try {
            urls = new URL[]{new URL(usedTests),new URL(usedClasses)};
        } catch (MalformedURLException e) { e.printStackTrace(); }
        URLClassLoader urlClassLoader = URLClassLoader.newInstance(urls);


        /*  // Test the urls used
        for(URL url:urls){
            System.out.println(url.getPath());
        }
        */
       /*  // Test class loading
        try {
            Class<?>temp = urlClassLoader.loadClass("Test.EditorEngineTest");
            System.out.println(temp.getName());
        } catch (ClassNotFoundException e) {
            e.printStackTrace();
        }
        */

        List<ClassDetail> testClasseNames = MainExec.getClassList(pathToTests);
        List<Class<?>> testClasses = new ArrayList<Class<?>>();

        for(ClassDetail cName:testClasseNames){
            String className = cName.getClassName();
            try {
                testClasses.add(urlClassLoader.loadClass(className));
            } catch (ClassNotFoundException e) { e.printStackTrace(); }

        }

        // converting the list<Class> to an array
        Class<?>[] classes = new Class[testClasses.size()];
        for(int i = 0;i<testClasses.size();i++){
            classes[i]=testClasses.get(i);
        }
        /*
        Class<?>[] classes = new Class[0];
        try {
            classes = new Class[]{urlClassLoader.loadClass("Test.EditorEngineTest")};
        } catch (ClassNotFoundException e) {
            e.printStackTrace();
        }
        */

        // Junit settings
        Request request = Request.classes(new Computer(), classes);
        Runner runner = request.getRunner();
        RunNotifier notifier = new RunNotifier();
        RequestStopListener listener = new RequestStopListener(notifier);

        // initializing the boolean returning the output of tests
        boolean toReturn;

        try {

            notifier.addFirstListener(listener);
            runner.run(notifier);
            toReturn = true;
        } catch (StoppedByUserException exc) {
            toReturn = false;
            System.out.println("Process stopped");
        }
        System.out.print("Tests executed: ");
        System.out.println(listener.getCount());


        return toReturn;
    }
}
