package fr.istic.logic;

import java.io.File;
import java.io.IOException;
import java.nio.file.Files;

public class FileBackup {

    private final String filePath;
    private byte[] content;

    public FileBackup (String filePath){
        this.filePath=filePath;
        File file = new File(filePath);
        try {
            this.content = Files.readAllBytes(file.toPath());
        } catch (IOException e) {
           e.printStackTrace();
        }
    }

    public void restoreClass() {
        File file = new File(this.filePath);
        try {
            Files.write(file.toPath(), content);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }



}
