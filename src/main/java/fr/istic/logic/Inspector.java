package fr.istic.logic;

import fr.istic.MainExec;
import fr.istic.dataStructure.ClassDetail;
import fr.istic.dataStructure.SurvivingMutant;
import fr.istic.mutator.*;
import javassist.*;
import javassist.bytecode.Opcode;

import java.io.IOException;
import java.net.MalformedURLException;
import java.util.ArrayList;
import java.util.List;
import java.util.jar.JarFile;

public class Inspector {

    private final String pathToClasses;
    private final String pathToTests;
    private final List<ClassDetail> classNames;
    private final ClassPool classPool;
    private final SurvivingMutant survivingMutant;

    public Inspector(String pathToClasses, String pathToTests, List<ClassDetail>classes, ClassPool pool, SurvivingMutant survivingMutant){
        this.pathToClasses=pathToClasses;
        this.pathToTests=pathToTests;
        this.classPool=pool;
        this.survivingMutant=survivingMutant;
        this.classNames=classes;
    }


	public void inspect() throws NotFoundException, CannotCompileException, IOException {


		// iteration on classes
		for(ClassDetail classe:this.classNames){
			String className=classe.getClassName();
			FileBackup save=new FileBackup(classe.getPathToClass());
			CtMethod[] ctMethods = getMethods(className);


			// iteration on methods
			for(CtMethod ctMethod : ctMethods){
				if( ctMethod.getReturnType().equals(CtClass.voidType)) {

					MutatorVoid mutatorVoid = new MutatorVoid();
					ClassPool childPool = generateClassPool(classPool);
					mutate(childPool,mutatorVoid,className,ctMethod.getName(), -1);
					childPool.get(className).writeFile(pathToClasses);
					runTests(className,ctMethod.getName(),"Void",-1);
					save.restoreClass();
				}

				else if (ctMethod.getReturnType().equals(CtClass.booleanType)) {
					MutatorTrue mutatorTrue = new MutatorTrue();
					ClassPool childPool = generateClassPool(classPool);
					mutate(childPool,mutatorTrue,className,ctMethod.getName(), -1);
					childPool.get(className).writeFile(pathToClasses);
					runTests(className,ctMethod.getName(),"True",-1);
					save.restoreClass();

					MutatorFalse mutatorFalse = new MutatorFalse();
					childPool = generateClassPool(classPool);
					mutate(childPool,mutatorFalse,className,ctMethod.getName(), -1);
					childPool.get(className).writeFile(pathToClasses);
					runTests(className,ctMethod.getName(),"False",-1);
					save.restoreClass();
				}

				MutatorOperator mutatorOperator = new MutatorOperator();
				byte[] code = ctMethod.getMethodInfo().getCodeAttribute().getCode();
				for(int i = 0; i< code.length; i++) {
                    if (code[i] == Opcode.IMUL || code[i] == Opcode.IDIV || code[i] == Opcode.IADD || code[i] == Opcode.ISUB) {
                    	ClassPool childPool = generateClassPool(classPool);
                        mutate(childPool, mutatorOperator,className,ctMethod.getName(), i);
                        childPool.get(className).writeFile(pathToClasses);
                        runTests(className,ctMethod.getName(),"Operator",i);
    					save.restoreClass();
                    }
				}
			}
		}

	}



    private CtMethod[] getMethods(String className) {

		CtClass ctClass = null;
		try {
			System.out.println(className);
			ctClass = this.classPool.get(className);
		} catch (NotFoundException e) { e.printStackTrace(); }


		return ctClass.getDeclaredMethods();
	}

	private void runTests(String className, String methodName, String type,int position) {
		boolean testPassed = TestExecutor.runTests(this.pathToClasses,this.pathToTests);
		System.out.println("retour de test: "+testPassed);
		if(testPassed){
		    this.survivingMutant.add(className,methodName,type,position);
		} else{
		    this.survivingMutant.justKilledAMutant();
        }
	}


	private ClassPool generateClassPool (ClassPool pool){

		ClassPool childPool = new ClassPool(pool);
		childPool.childFirstLookup = true;
		try {
			childPool.appendClassPath(pathToClasses);
		} catch (NotFoundException e) { e.printStackTrace(); }

		return childPool;
	}

	private ClassPool mutate(ClassPool classPool, MutatorPrototype mutator, String className, String methodName , int position){
		return mutator.mutate(classPool,className,methodName,position);
	}


}