package fr.istic.dataStructure;

import fr.istic.dataStructure.MutantEntry;

import java.util.ArrayList;
import java.util.List;

public class SurvivingMutant {


    private ArrayList<MutantEntry> mutantEntryList;
    private int killedMutantCount;

    public SurvivingMutant() {
        this.killedMutantCount = 0;
        this.mutantEntryList= new ArrayList<MutantEntry>();
    }

    public void add(String className, String methodName, String type, int position) {
        MutantEntry mutantEntry = new MutantEntry(className,methodName,type,position);
        this.mutantEntryList.add(mutantEntry);
    }

    public void displayReport(){
        for(MutantEntry mutant:this.mutantEntryList){
            System.out.println("-------------------------------------------");

            System.out.println("Mutant class:       "+mutant.getClassName());
            System.out.println("Mutant method name: "+mutant.getMethodName());
            System.out.println("Mutation type:      "+mutant.getType());
            System.out.println("Mutation position:  "+mutant.getPosition());


        }
        System.out.println("-------------------------------------------");
        System.out.println("Count of killed mutants: "+this.killedMutantCount);
        System.out.println("Surviving mutants to report: "+this.mutantEntryList.size());
    }

    public int sizeOfMutantList(){
        return this.mutantEntryList.size();
    }

    public List<MutantEntry> getMutantEntryList(){
        return this.mutantEntryList;
    }

    public void justKilledAMutant(){
        this.killedMutantCount++;
    }

    public  int getKilledMutantCount(){
        return this.killedMutantCount;
    }

}
