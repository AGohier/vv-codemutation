package fr.istic.dataStructure;

public class MutantEntry {

    final private String className;
    final private String methodName;
    final private String type;
    final private int position;

    public MutantEntry(String className, String methodName, String type, int position){
        this.className= className;
        this.methodName=methodName;
        this.type= type;
        this.position=position;
    }

    public String getClassName() {
        return className;
    }

    public String getMethodName() {
        return methodName;
    }

    public String getType() {
        return type;
    }

    public int getPosition() {
        return position;
    }

}
