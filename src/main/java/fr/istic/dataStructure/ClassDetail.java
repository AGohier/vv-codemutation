package fr.istic.dataStructure;

public class ClassDetail {

    private final String pathToClass;
    private final String className;

    public ClassDetail(String pathToClass, String className){
        this.pathToClass =pathToClass;
        this.className=className;
    }

    public String getPathToClass() {
        return pathToClass;
    }

    public String getClassName() {
        return className;
    }
}
