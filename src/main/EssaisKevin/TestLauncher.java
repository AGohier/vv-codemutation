import org.junit.internal.TextListener;
import org.junit.runner.JUnitCore;

import javassist.ClassPool;
import javassist.CtClass;
import javassist.CtMethod;
import javassist.Loader;
import javassist.Translator;

public class TestLauncher {
	
    public static void main(String... args) throws Throwable {
    /*
        ClassPool pool = ClassPool.getDefault();
        final String folder = "./manipulation-target/target/classes/";
        pool.appendClassPath(folder);
        CtClass mainClass = pool.getCtClass("Main");
        CtMethod[] methods = mainClass.getMethods();
	*/
    	ClassPool pool = ClassPool.getDefault();
    	Loader classLoader = new Loader(pool);
    	Class testClass = pool.getCtClass("SomeTests").toClass();
    	//String[] runClass = new String[1];
    	//runClass[0] = "SomeTests";
    	JUnitCore junit = new JUnitCore();
    	junit.addListener(new TextListener(System.out));
    	junit.run(testClass);
    	//classLoader.run(runClass);
        
    }
}