
public class TestTarget {
	
	private String helloToYou;
	
	public TestTarget() {
		helloToYou = "";
	}

	public String getHelloToYou() {
		return helloToYou;
	}

	public void setHelloToYou(String helloToYou) {
		this.helloToYou = helloToYou;
	}
	
	public void sayHello() {
		String message = "Hello" + " " + helloToYou + "!";
		System.out.println(message);
	}

}
