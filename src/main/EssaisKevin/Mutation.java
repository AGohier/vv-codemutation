import javassist.ClassPool;
import javassist.CtClass;
import javassist.CtMethod;
import javassist.bytecode.Opcode;

public class Mutation {
	
    public static void main(String... args) throws Throwable {
  
    	// Création class pool
        ClassPool pool = ClassPool.getDefault();
        
        // Ajout du chemin vers les fichiers .class
        final String folder = "./target/classes/";
        pool.appendClassPath(folder);
        
        // Récupération d'une classe
        CtClass mainClass = pool.getCtClass("TestTarget");
        
        // Récupération d'une méthode
        CtMethod method = mainClass.getDeclaredMethod("sayHello");
        
        // Récupération du bytecode
        byte[] code = method.getMethodInfo().getCodeAttribute().getCode();
        
        // Exemple de mutation
        for(int i = 0; i< code.length; i++) {
            if(code[i] == Opcode.DMUL) {
                code[i] = Opcode.DADD;
            }
        }
    }
    
    // Une fonction pour muter le premier opérateur trouvé.
    // A adapter pour le projet, évidemment.
    private void mutateOperator(CtMethod method) {
    	byte[] code = method.getMethodInfo().getCodeAttribute().getCode();
        for(int i = 0; i< code.length; i++) {
            if(code[i] == Opcode.DMUL) {
                code[i] = Opcode.DDIV;
                return;
            }
            else if(code[i] == Opcode.DDIV) {
                code[i] = Opcode.DMUL;
                return;
            }
            else if(code[i] == Opcode.DADD) {
                code[i] = Opcode.DSUB;
                return;
            }
            else if(code[i] == Opcode.DSUB) {
                code[i] = Opcode.DADD;
                return;
            }
        }
    }
	

}
