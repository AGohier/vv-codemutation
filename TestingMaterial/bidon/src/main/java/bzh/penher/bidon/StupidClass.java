package bzh.penher.bidon;

import bzh.penher.bidon.dto.DummyPojo;

public class StupidClass {
    public boolean returnTrue() {
        return true;
    }

    public void resetPojo(DummyPojo dto) {
        dto.setName("");
        dto.setId(0);
    }

    public int multiply(int a, int b) {
        return a * b;
    }

    public int add(int a, int b) {
        return a + b;
    }

    public int substract(int a, int b) {
        return a - b;
    }

    public double divide(double a, double b) {
        return a / b;
    }
}
